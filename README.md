# 8Puzzle

This project was part of a third year university Artificial Intelligence assignment for the _B.Sc. Information Technology_ course.
This project utilises the _Java Swing_ framework, at the time of creation I did not know of _JavaFX_ or _Qt_ and used _JFormDesigner_. I will update the project to the Qt framework in the future.
The sound effects aren't always working correctly, probably because of the deprecated Java libraries I am using. This application does not work well on Windows with GUI so I have created a CLI version for Windows, works well with macOS and Linux.

I attempted to implement the A\* algorithm, I had some success but not complete.

E-Mail: zander.labuschagne@protonmail.ch

Copyright (C) 2017 Zander Labuschagne. This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 2 as published by the Free Software Foundation.
